package Zadanie32_SumaCyfrStringa;

import java.util.Scanner;

public class Zadanie32Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        int suma = 0;

        //sumuje cyfry w podanym tekscie
        System.out.println("Podaj ciag: ");
        char[] ciag = skan.nextLine().toCharArray();
        for (int i = 0; i < ciag.length ; i++) {
            if(ciag[i] >= 48 && ciag[i] <= 57){
                suma += (int)ciag[i] - 48;
            }
        }
        System.out.println("Suma cyfr w ciągu to: " + suma);
    }
}
