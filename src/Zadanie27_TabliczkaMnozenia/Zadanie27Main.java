package Zadanie27_TabliczkaMnozenia;

import java.util.Scanner;

public class Zadanie27Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);

        //wczytaj liczbe do ktorej liczymy tabliczke
        System.out.println("Podaj liczbe do mnozenia: ");
        int liczba = skan.nextInt();

        for (int i = 1; i <= liczba; i++) {
            System.out.println(liczba + " * " + i + " = " + liczba * i);
        }
    }
}
