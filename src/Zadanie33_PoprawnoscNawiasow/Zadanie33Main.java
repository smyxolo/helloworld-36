package Zadanie33_PoprawnoscNawiasow;

import java.util.Scanner;

public class Zadanie33Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        char[] ciag = skan.nextLine().toCharArray();
        boolean poprawne = true;

        // "(" : 40
        // ")" : 41

        int otwarte = 0;
        int zamkniete = 0;


        for (int i = 0; i < ciag.length; i++) {

            if((int)ciag[i] == 40){
                otwarte++;

            }

            if((int)ciag[i] == 41){

                zamkniete++;

            }

            if(zamkniete > otwarte) {
                poprawne = false;
                break;
            }
        }
        if(otwarte != zamkniete) poprawne = false;
        System.out.println("Poprawne: " + poprawne);
    }
}
