package Zadanie34_SzyfrCezara;

import java.util.Scanner;

public class Zadanie34Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Podaj ciag znakow: ");
        char[] ciag = skan.nextLine().toCharArray();
        System.out.println("Podaj przesuniecie: ");
        int przesuniecie = skan.nextInt();

        System.out.println();
        // az 97:122

        for (int i = 0; i < ciag.length; i++) {
            for (int j = 1; j <= przesuniecie; j++) {
                if((int)ciag[i] < 122){
                    ciag[i] += 1;
                }
                if((int)ciag[i] == 122){
                    ciag[i] = 97;
                }
            }
        }

        for (int i = 0; i < ciag.length; i++) {
            System.out.print(ciag[i]);
        }
        //printuj

    }
}