package Zadanie7_CelsjuszDoFahrenheit;

public class MainZadanie7 {
    public static void main(String[] args) {

        float celsjusz = 36.6f;
        float fahrenheit = 1.8f * celsjusz + 32.0f;
        System.out.println(celsjusz + " stopni Celsjusza to " + fahrenheit + " stopni Fahrenheita.");

    }
}
