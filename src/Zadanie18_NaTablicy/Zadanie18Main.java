package Zadanie18_NaTablicy;

import java.util.Scanner;

public class Zadanie18Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);

        //zapytaj o ilosc elementow w tablicy
        System.out.println("Podaj liczbe elementow w tablicy: ");
        int ilosc = skan.nextInt();

        int tablica[] = new int[ilosc];

        //wczytaj wartosci do tablicy
        for(int i = 0; i < ilosc; i++){
            System.out.println("Podaj " + (i + 1) + " element tablicy.");
            tablica[i] = skan.nextInt();
        }

        //wypisz tablice
        System.out.println("Twoja tablica to: ");
        for(int i = 0; i < ilosc; i++){
            System.out.print(tablica[i]+ " ");
        }
        System.out.println("\n \n");

        //znajdz najwyzsza i najnizsza wartosc i wypisz ja
        int najwyzsza = tablica[0];
        int najnizsza = tablica[0];

        for(int i = 0; i < ilosc; i++){
            if(tablica[i] > najwyzsza){
                najwyzsza = tablica[i];
            }
            if(tablica[i] < najnizsza){
                najnizsza = tablica[i];
            }
        }
        System.out.println("Najwyzsza wartosc w tablicy: " + najwyzsza);
        System.out.println("Najnizsza wartosc w tablicy: " + najnizsza);
        System.out.println();

        //podaj sume skrajnych wartosci
        System.out.println("Suma skrajnych wartosci to: " + (najnizsza + najwyzsza));
        System.out.println();

        //wylicz i wypisz srednia arytmetyczna wszystkich wartosci
        int suma = 0;
        for(int i = 0; i < ilosc; i++){
            suma += tablica[i];
        }
        System.out.println("Srednia arytmetyczna wszystkich wartosci to:\n"+ (double)((double)suma / (double)ilosc));





    }
}
