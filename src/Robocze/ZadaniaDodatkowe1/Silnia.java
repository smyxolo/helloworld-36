package Robocze.ZadaniaDodatkowe1;

import java.util.Scanner;

public class Silnia {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);

        System.out.println("Podaj liczbe dla ktorej chcesz obliczyc silnie: ");
        int liczba = skan.nextInt();
        System.out.println();
        int wynik = 1;

        for(int i = 1; i <= liczba; i++){
            wynik *= i;
        }
        System.out.println("Silnia liczby " + liczba + " to:\n" + wynik);
    }
}
