package Robocze.ZadaniaDodatkowe1;

import java.util.Scanner;

public class ZnakiWStringu {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Podaj ciag znakow:");
        String testit = skan.nextLine();
        System.out.println("Dlugosc ciagu znakow to: " + testit.length());
        char[] slowo1 = testit.toCharArray();
        System.out.println();
        int[] listaZnakow = new int[128];
        for (int litera : slowo1) {
            listaZnakow[litera] += 1;
//            System.out.print((char) litera);
//            System.out.print(": ");
//            System.out.print(listaZnakow[litera]);
//            System.out.print("\n");
        }
        for (int i=64; i<122; i++) {
            if (listaZnakow[i] > 0) {
                System.out.print(((char) i));
                System.out.print(": ");
                System.out.print(listaZnakow[i] + "\n");
            }
        }
    }

}
