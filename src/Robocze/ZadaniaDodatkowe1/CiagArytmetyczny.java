package Robocze.ZadaniaDodatkowe1;


import java.util.*;

public class CiagArytmetyczny {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);

        //pobierz z konsoli pierwszy wyraz ciagu
        System.out.println("Podaj pierwszy wyraz ciagu: ");
        double a1 = skan.nextDouble();

        //pobierz z konsoli roznice ciagu
        System.out.println("Podaj roznice ciagu: ");
        double r = skan.nextDouble();

        //pobierz parametr: ile elementow ma zawierac suma ciagu?
        System.out.println("Ile elementow ma zawierac suma:");
        int n = skan.nextInt();

        //oblicz sume ciagu i wypisz ja w konsoli
        double suma = a1 + (r * n);

        System.out.println("Suma " + n + " elementow Twojego ciagu to: ");
        System.out.println(suma);
    }
}
