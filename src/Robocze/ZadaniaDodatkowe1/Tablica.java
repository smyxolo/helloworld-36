package Robocze.ZadaniaDodatkowe1;

import java.util.*;

public class Tablica {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
//        ArrayList<Integer> tablica = new ArrayList<Integer>();
        int[] tablica ;

        //podaj wielkosc tablicy
        System.out.println("Podaj ilosc elementow tablicy: ");
        int wielkosc_tab = skan.nextInt();

        tablica = new int[wielkosc_tab];
        //podaj wszystkie elementy tablicy
        for(int j = 0; j < wielkosc_tab; j++){
            System.out.println("Podaj kolejny element tablicy: ");
//            tablica.add(skan.nextInt());
            tablica[j] = skan.nextInt();
        }
        //wypisz tablice
        System.out.println("Twoja tablica to:");
        for(int i = 0; i < wielkosc_tab; i++){
            System.out.print(tablica[i]+ " ");
        }
//        System.out.println(tablica);
        System.out.println();

        //oblicz sume liczb w tablicy i wypisz ja
        int suma = 0;

        for(int i = 0; i < wielkosc_tab; i++){
            suma += tablica[i];
        }
        System.out.println("Suma elementow to:");
        System.out.println(suma);
        System.out.println();

        //oblicz srednia liczb w tablicy i wypisz ja
        double srednia = suma / wielkosc_tab;

        System.out.println("Srednia wartosc to:");
        System.out.println(srednia);
        System.out.println();

        //oblicz iloczyn liczb w tablicy i wypisz wynik
        int iloczyn = 1;

        for(int k = 0; k < wielkosc_tab; k++){
            iloczyn *= tablica[k];
        }
        System.out.println("Iloczyn wartosci to: ");
        System.out.println(iloczyn);
        System.out.println();






        //Iterator iterator = zbiorWartosci.iterator()
    }
}
