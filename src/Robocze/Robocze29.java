package Robocze;

import java.util.Scanner;

public class Robocze29 {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Podaj ciag znakow a ja zlicze wystepowanie ostatniego:");
        char[] ciag = skan.nextLine().toCharArray();

        int ostatni = (ciag.length) - 1;
        int wystepowanie = 0;

        for (int i = 0; i < ciag.length; i++) {
            if(ciag[i] == ciag[ostatni]){
                wystepowanie++;
            }
        }
        System.out.println("'" + ciag[ostatni] + "' wystepuje tutaj " + wystepowanie + " razy.");
    }
}
