package Robocze;

import java.util.Scanner;

public class Robocze31 {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Podaj ciag znakow: ");
        char[] ciag = skan.nextLine().toCharArray();
        int iteratormax = 0;

        //ustawic max iteracji
        if (ciag.length % 2 == 0){
            iteratormax = ciag.length / 2;
        }
        else {
            iteratormax = (ciag.length - 1) /2;
        }

        //ustalic czy palindrom

        int palindrom = 0;
        boolean palindrom1;

        for (int i = 0; i < iteratormax; i++) {
            if (ciag[i] == ciag[ciag.length - 1 - i]) {
                palindrom++;
            }
        }
        if(palindrom == iteratormax){
            palindrom1 = true;
        }
        else {
            palindrom1 = false;
        }
        System.out.println("Palindrom: " + palindrom1);
    }
}
