package Zadanie24_Lotto;

import java.util.Random;

public class Zadanie24Main {
    public static void main(String[] args) {

        Random random = new Random();
        int[] tablica = new int[10];
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        double suma = 0;


        System.out.println("Wylosowane liczby: ");
        for (int i = 0; i < 10; i++) {
            tablica[i] = (random.nextInt(21)-10);
            System.out.print(tablica[i] + " ");
            suma += tablica[i];
            if(tablica[i] < min){
                min = tablica[i];
            }
            if(tablica[i] > max){
                max = tablica[i];
            }

        }
        System.out.println();
        System.out.println("Min: " + min);
        System.out.println("Max: " + max);
        System.out.println("Srednia: "+ (suma / 10));

        //wieksze/mniejsze od sredniej

        int wiekszych = 0;
        int mniejszych = 0;
        for (int i = 0; i < 10 ; i++) {
            if (tablica[i] < suma /10){
                mniejszych++;
            }
            if(tablica[i] > suma / 10){
                wiekszych++;
            }
        }
        System.out.println("Wiekszych od sr.: " + wiekszych);
        System.out.println("Mniejszych od sr.: " + mniejszych);
        System.out.println();
        System.out.println("Liczby w odwrotnej kolejnosci: ");
        for (int i = 9; i >= 0; i--) {
            System.out.print(tablica[i] + " ");
        }

    }
}
