package Zadanie36_RownanieKwadratowe;

import java.util.Scanner;

public class Zadanie36Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Podaj liczby a, b, c dla rownania w formacie:");
        System.out.println("ax^2  + bx + c");
        String linia = skan.nextLine();

        try {
            double a = Double.parseDouble(linia.split(" ", 3)[0]);
            double b = Double.parseDouble(linia.split(" ", 3)[1]);
            double c = Double.parseDouble(linia.split(" ", 3)[2]);
            double delta = Math.pow(b, 2) - 4*a*c;
            double x;
            double x1;
            double x2;

            if(a == 0){
                throw new IllegalArgumentException("To nie jest rownanie kwadratowe.");
            }

            if(delta == 0){
                x = (-b)/2*a;
                if(x == -0.0){
                    x = Math.abs(x);
                }
                System.out.println("Rownanie ma jeden pierwiastek:\n" + "x = " + x);
            }
            else if (delta > 0){
                x1 = (-1*b - Math.sqrt(delta))/2*a;
                x2 = (-1*b + Math.sqrt(delta))/2*a;
                System.out.println("Rownanie ma dwa pierwiastki:\n" + "x1 = " + x1 + "\nx2 = " + x2);
            }
            else {
                System.out.println("Rownanie nie ma rozwiazan.");
            }
            System.out.println("Delta = " + delta);

        }
        catch (NumberFormatException nfe){
            System.out.println("Nieprawidlowy format jednej lub wiecej liczb.");
        }
        catch (IllegalArgumentException iae){
            System.out.println("To nie jest rownanie kwadratowe.");
        }

    }
}
