package Zadanie12_CzyPodzielne;

import java.util.Scanner;

public class Zadanie12Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);

        //pobierz liczbe
        System.out.println("Podaj liczbe calkowita dodatnia:");
        int liczba = skan.nextInt();

        System.out.println("Liczby parzyste mniejsze niz podana:");

        if(liczba > 1){
            for(int i = 1; i < liczba; i += 2){
                System.out.println(i);
            }
        }
        else {
            System.out.println("Nie ma liczb nieparzystych dodatnich mniejszych od podanej");
        }

        System.out.println("________________________________________________");
        System.out.println("'3 -> 100', podzielne przez 5");
        for(int i = 3; i <= 100; i++){
            if(i % 5 == 0){
                System.out.println(i);
            }
        }
        System.out.println("________________________________________________");

        System.out.println("Podaj dolna granice przedzialu: ");
        int dolna = skan.nextInt();
        System.out.println("Podaj gorna granice przedzialu: ");
        int gorna = skan.nextInt();
        System.out.println("Przez jaka liczbe maja byc podzielne");
        int modulo = skan.nextInt();

        for (int i = dolna; i <= gorna; i++) {
            if (i % modulo == 0) {
                System.out.println(i);
            }


        }

        System.out.println("podaj dolny zakres przedzialu: ");
        int min = skan.nextInt();
        System.out.println("podaj gorny zakres przedzialu: ");
        int max = skan.nextInt();

        if(min >  max){
            int temp = min;
            min = max;
            max = temp;
        }

        System.out.println("Liczby podzielne przez 6 z przedzialu: " + min + " do " + max);
        for (int i = min; i <= max; i++) {
            if(i % 6 == 0) System.out.println(i);
        }

    }
}