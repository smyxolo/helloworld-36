Napisać program, który pobiera od użytkownika liczbę całkowitą dodatnią,
a następnie wyświetla na ekranie kolejno wszystkie liczby niepatrzyste nie większe od podanej liczby.

Przykład, dla 15 program powinien wyświetlić 1, 3, 5, 7, 9, 11, 13, 15.
        B)
            Napisać program, który wykorzystuje pętle i wypisuje liczby podzielne przez 3 lub przez 5 w przedziale od 3 do 100 (3,5,6,9,10,...)
        C)
            Napisać program, który wykorzystuje pętle i wczytuje od użytkownika przedział liczbowy
            (użytkownik ma podać dolną granicę zakresu i górną) (początek i koniec przedziału),
            a następnie wypisz wszystkie liczby z tego przedziału podzielne przez 6.