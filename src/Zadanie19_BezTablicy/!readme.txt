Zadanie 19*:
    Napisać program, który pobiera od użytkownika ciąg liczb całkowitych. Pobieranie danych kończone jest podaniem wartości 0 (nie wliczana do danych).
    W następnej kolejności program powinien wyświetlić sumę największej oraz najmniejszej z podanych liczb oraz ich średnią arytmetyczną.
Wskazówka:
    Nie jest potrzebna tablica.
Wskazówka:
    Czytaj liczby tak długo aż wczytana liczba nie jest 0 (while).