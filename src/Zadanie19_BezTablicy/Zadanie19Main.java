package Zadanie19_BezTablicy;

import java.util.Scanner;

public class Zadanie19Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);

        //wylicz najwieksza i najmniejsza z podawanych liczb
        //zero konczy program

        System.out.println("Aby zakonczyc porownywanie wcisnij '0'.");
        System.out.println("Podaj pierwsza liczbe.");
        int aktualna = skan.nextInt();

        int najnizsza = aktualna;
        int najwyzsza = aktualna;
        int suma = aktualna;
        int ilosc = 1;

        while(aktualna != 0){
            System.out.println("Podaj kolejna liczbe:");
            aktualna = skan.nextInt();

            ilosc++;
            suma += aktualna;

            if (aktualna > najwyzsza){
                najwyzsza = aktualna;
            }
            if (aktualna < najnizsza){
                najnizsza = aktualna;
            }

        }
        System.out.println();
        System.out.println("Najwyzsza z podanych to: \n" + najwyzsza + "\n");
        System.out.println("Najnizsza z podanych to: \n" + najnizsza + "\n");
        System.out.println("Srednia wszystkich podanych liczb to: \n" + (double)((double)suma/(double)ilosc));

    }
}
