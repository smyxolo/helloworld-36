package Zadanie35_WeryfikujInput.TryCatch;

import java.util.Scanner;

public class Zadanie35Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        double pierwsza;
        double druga;
        double wynik = 0;

        do {
            System.out.println(wynik);
            System.out.println("Podaj dzielna i dzielnik:");
            System.out.println("(wpisz 'quit' aby wyjsc)");
            String[] linia = skan.nextLine().split(" ", 2);
            if(linia[0].equals("quit")){
                break;
            }
            try {
                if(Double.parseDouble(linia[1]) == (double)0){
                    throw new IllegalArgumentException("Dzielnik = 0");
                }
                pierwsza = Double.parseDouble(linia[0]);
                druga = Double.parseDouble(linia[1]);
                wynik = pierwsza / druga;
            }
            //jezeli format nieprawidlowy
            catch (NumberFormatException nfe){
                System.out.println("Nieprawidlowy format jednej lub obu liczb.");
                continue;
            }
            //jezeli tylko jeden element
            catch (ArrayIndexOutOfBoundsException aioobe){
                System.out.println("Podales jeden element");
                continue;
            }
            catch (IllegalArgumentException IAE){
                System.out.println("Nie mozesz dzielic przez 0.");
            }
        } while (skan.hasNextLine());
    }
}
