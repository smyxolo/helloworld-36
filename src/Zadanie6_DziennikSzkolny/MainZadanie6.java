package Zadanie6_DziennikSzkolny;

public class MainZadanie6 {
    public static void main(String[] args) {
        int ocena_matematyka = 3;
        int ocena_chemia = 5;
        int ocena_polski = 4;
        int ocena_angielski = 5;
        int ocena_wos = 2;
        int ocena_informatyka = 1;

        double srednia_wszystkich = (ocena_matematyka + ocena_angielski + ocena_chemia + ocena_polski + ocena_wos + ocena_informatyka);
        srednia_wszystkich = srednia_wszystkich / 6;
        System.out.println("Srednia wszystkich ocen to: "+ srednia_wszystkich);

        double srednia_scislych = (ocena_matematyka + ocena_chemia + ocena_informatyka);
        srednia_scislych = srednia_scislych / 3;
        System.out.println("Srednia ocen z przedmiotow scislych to: " + srednia_scislych);

        double srednia_human = (ocena_polski + ocena_angielski + ocena_wos);
        srednia_human = srednia_human / 3;
        System.out.println("Srednia ocen z przedmiotow humanistycznych to: " + srednia_human);

        if (ocena_matematyka == 1){
            System.out.println("Ocena z matematyki jest niedostateczna");
        }
        if (ocena_chemia == 1){
            System.out.println("Ocena z chemii jest niedostateczna");
        }
        if (ocena_polski == 1){
            System.out.println("Ocena z polskiego jest niedostateczna");
        }
        if (ocena_angielski == 1){
            System.out.println("Ocena z angielskiego jest niedostateczna");
        }
        if (ocena_wos == 1){
            System.out.println("Ocena z wosu jest niedostateczna");
        }
        if (ocena_informatyka == 1){
            System.out.println("Ocena z informatyki jest niedostateczna");
        }
        if (srednia_scislych == 1){
            System.out.println("Srednia ocen z przedmiotow scislych jest niedostateczna");
        }
        if (srednia_human == 1) {
            System.out.println("Srednia ocen z przedmiotow humanistycznych jest niedostateczna");
        }

    }
}
