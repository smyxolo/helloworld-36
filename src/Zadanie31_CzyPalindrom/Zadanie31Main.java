package Zadanie31_CzyPalindrom;

import java.util.Scanner;

public class Zadanie31Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Podaj ciag znakow: ");
        char[] ciag = skan.nextLine().toCharArray();
        int palindrom = 0;
        boolean palindrom1;

        //jesli ma parzysta ilosc znakow:
        if(ciag.length % 2 == 0){
            for (int i = 0; i < ciag.length / 2; i++) {
                if (ciag[i] == ciag[ciag.length - 1 - i]){
                    palindrom++;
                }
            }
            if(palindrom == ciag.length / 2){
                palindrom1 = true;
            }
            else {
                palindrom1 = false;
            }
        }

        //jesli ma nieparzysta ilosc znakow:
        else {
            for (int i = 0; i < (ciag.length - 1) / 2; i++) {
                if (ciag[i] == ciag[ciag.length - 1 - i]){
                    palindrom++;
                }
            }
            if(palindrom == (ciag.length - 1) / 2){
                palindrom1 = true;
            }
            else {
                palindrom1 = false;
            }
        }
        System.out.println("Palindrom: " + palindrom1);
    }
}
