package Zadanie17_Potegi2;

import java.util.Scanner;

public class Zadanie17Main {
    public static void main(String[] args) {

        Scanner skan = new Scanner(System.in);

        System.out.println("Podaj liczbe gornej granicy: ");
        int liczba = skan.nextInt();

        int iterator = 0;

        while((Math.pow(2, iterator)) < liczba){
            System.out.print((int)(Math.pow(2, iterator)) + " ");
            iterator++;
        }


    }
}
