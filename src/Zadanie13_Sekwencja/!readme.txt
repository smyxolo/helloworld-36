Zadanie 13:
    Napisać program pobierający od użytkownika dwie liczby całkowite A oraz B, A < B,
    a następnie wyznaczający sumę ciągu liczb od A do B, czyli sumę ciągu (A,A + 1,...,B).
    bliczenia należy wykonać dwukrotnie stosując kolejno pętle: while, for.
Przykład: Dla A = 4 i B = 11 program powinien wyświetlić: 60 60