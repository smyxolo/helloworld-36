package Zadanie13_Sekwencja;

import java.util.Scanner;

public class Zadanie13Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);

        //pobierz dwie liczby calkowite

        System.out.println("Podaj pierwsza liczbe calkowita a:");
        int a = skan.nextInt();
        System.out.println("Podaj druga liczbe calkowita b:");
        int b = skan.nextInt();

        int suma = 0;

        for(int i = a; i <= b; i++){
            suma += i;

        }
        System.out.println("Suma ciagu1 = ");
        System.out.println(suma);
        System.out.println();

        suma = 0;
        int iterator = a;

        while (iterator <= b){
            suma += iterator;
            iterator++;
        }
        System.out.println("Suma ciagu1 = ");
        System.out.println(suma);
        System.out.println();
    }
}
