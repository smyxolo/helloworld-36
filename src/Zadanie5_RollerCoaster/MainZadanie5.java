package Zadanie5_RollerCoaster;

public class MainZadanie5 {
    public static void main(String[] args) {
        int waga = 74;
        int wzrost = 146;
        int wiek = 80;

        checkParameters(waga, wzrost, wiek);
        System.out.println();
        System.out.println("_____________inne_podejscie____________");

        // drugie podejście______________________________________

        // kolejnosc: !, &&, ||.

        String wchodzisz = (waga <= 180 && wzrost >= 150 &&(wiek >= 10 && wiek < 80)) ?
                "Witamy na pokladzie!" : "Niestety nie możesz wejść ponieważ: ";
        System.out.println(wchodzisz);

        boolean duzaWaga = (waga > 180);
        boolean malyWzrost = (wzrost < 150);
        boolean mlodyWiek = (wiek < 10);
        boolean starczyWiek = (wiek > 80);

        if(duzaWaga){
            System.out.println("- wazysz zbyt duzo");
        }
        if(malyWzrost){
            System.out.println("- jestes zbyt niski");
        }
        if(mlodyWiek){
            System.out.println("- jestes zbyt mlody");
        }
        if(starczyWiek){
            System.out.println("- nie wpuszczamy osob w tak powaznym wieku");
        }


    }

    public static void checkParameters(int waga, int wzrost, int wiek) {
        if (waga <= 180 && wzrost >= 150 && (10 <= wiek && wiek < 80)){
            System.out.println("Zapnij pasy.");
        }
        else {
            if(waga >= 180){
                System.out.println("Wybacz, jesteś zbyt ciężki.");
            }
            if(wzrost < 150){
                System.out.println("Wybacz, jesteś zbyt niski.");
            }
            if(wiek < 10){
                System.out.println("Wybacz, jesteś zbyt młody.");
            }
            if(wiek >= 80){
                System.out.println("Wybacz, jesteś zbyt stary.");
            }
            System.out.println("Nie możesz wejść!");
        }
    }
}
