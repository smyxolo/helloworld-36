Zadanie 3:

Zadeklaruj 3 zmienne - zmienna 'a', zmienna 'b', zmienna 'c'.
Przypisz jej 3 wartości - ważne żeby były różne.
Następnie wykonaj na nich następujące działania:
            a) przepisz wartości:
             - do zmiennej 'a' przypisz wartość 'b',
             - do zmiennej 'b' przypisz wartość 'c',
             - do zmiennej 'c' przypisz wartość 'a'.

            b) wypisz wartości na ekran.