package Zadanie3_KaruzelaZmiennych;

public class MainZadanie3 {
    public static void main(String[] args) {
        int a = 1;
        int b = 2;
        int c = 3;
        int temp;

        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("c = " + c);

        temp = a;
        a = b;
        b = c;
        c = temp;

        System.out.println();

        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("c = " + c);
    }
}
