package Zadanie20_Zgadula;

import java.util.Random;
import java.util.Scanner;

public class Zadanie20Main {
    public static void main(String[] args){

        Scanner skan = new Scanner(System.in);
        Random random = new Random();

        int liczba = (int)Math.ceil(Math.random()*100);
        // lub int liczba2 = random.nextInt();
        //System.out.println("/robocze/ Liczba do zgadniecia to: " + liczba);

        System.out.println("Zgadnij liczbe od 1 do 100:");
        int strzal = skan.nextInt();
        int proba = 1;

        while (strzal != liczba) {
            if (strzal > liczba){
                System.out.println("Podałeś za dużą wartość.");
            }
            else {
                System.out.println("Podałeś za małą wartość.");

            }
            proba += 1;
            System.out.println("Zgaduj dalej:");
            strzal = skan.nextInt();
        }
        System.out.println("Gratulacje!");
        System.out.println("Zgadles za " + proba + " razem!");
    }
}
