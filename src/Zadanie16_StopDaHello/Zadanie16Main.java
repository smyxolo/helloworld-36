package Zadanie16_StopDaHello;

import java.util.Scanner;

public class Zadanie16Main {
    public static void main(String[] args) {
        System.out.println("Podaj liczbe:");
        Scanner skan = new Scanner(System.in);

        int liczba = skan.nextInt();
        while (liczba > 0){
            System.out.println("Hello, World");
            liczba = skan.nextInt();
        }
    }
}
