package Zadanie29_OstatniCzar;

import java.util.Scanner;

public class Zadanie29Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Podaj ciag znakow a ja zlicze wystepowanie ostatniego:");
        char[] ciag = skan.nextLine().toCharArray();

        int wystepowanie = 0;

        for (int i = 0; i < ciag.length; i++) {
            if(ciag[i] == ciag[ciag.length - 1]){
                wystepowanie++;
            }
        }
        System.out.println("'" + ciag[ciag.length - 1] + "' wystepuje tutaj " + wystepowanie + " razy.");
    }
}
