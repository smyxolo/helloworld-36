Zadanie 29:
     Napisać program, który wczytuje od użytkownika ciąg znaków, a następnie wy-
     świetla informację o tym ile razy w tym ciągu powtarza się jego ostatni znak.
     Przykład, dla ciągu „Abrakadabra” program powinien wyświetlić 4, ponieważ
     ostatnim znakiem jest literka „a”, która występuje w podanym ciągu łącznie 4
     razy