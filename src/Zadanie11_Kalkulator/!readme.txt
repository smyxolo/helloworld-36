Zadanie 11:
 Napisać program realizujący funkcje prostego kalkulatora,
 pozwalającego na wykonywanie operacji dodawania, odejmowania, mnożenia i dzielenia na dwóch liczbach rzeczywistych.
 Program ma identyﬁkować sytuację wprowadzenia błędnego symbolu działania oraz próbę dzielenia przez zero.
 Zastosować instrukcję if else do wykonania odpowiedniego działania w zależności od wprowadzonego symbolu operacji.

 Scenariusz działania programu:
a) Program wyświetla informację o swoim przeznaczeniu.
b) Wczytuje pierwszą liczbę.
c) Wczytuje symbol operacji arytmetycznej: +, -, *, /.
d) Wczytuje drugą liczbę.
e) Wyświetla wynik lub - w razie konieczności - informację o niemożności wykonania działania.