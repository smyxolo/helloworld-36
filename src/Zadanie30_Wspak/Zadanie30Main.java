package Zadanie30_Wspak;

import java.util.Scanner;

public class Zadanie30Main {
    public static void main(String[] args){
        Scanner skan = new Scanner(System.in);

        System.out.println("Podaj lancuch znakow");
        char[] lancuch = skan.nextLine().toCharArray();

        System.out.println("Twoj lancuch odwrotnie to: ");
        for (int i = lancuch.length - 1; i >= 0; i--) {
            System.out.print(lancuch[i]);
        }

    }
}
