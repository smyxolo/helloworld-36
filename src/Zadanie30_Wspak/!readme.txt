Zadanie 30 A:
    Napisać program, który wczytuje od użytkownika ciąg znaków, a następnie tworzy
    łańcuch będący odwróceniem podanego łańcucha i wyświetla go na ekranie.
    Przykładowo, dla łańcucha „Kot” wynikiem powinien być łańcuch „toK”.