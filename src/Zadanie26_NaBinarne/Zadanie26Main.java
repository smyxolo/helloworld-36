package Zadanie26_NaBinarne;

import java.util.Scanner;

public class Zadanie26Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Podaj liczbe do przekonwertowania:");
        int decymal = skan.nextInt();
        int[] binar = new int[32];

        //ustalenie znaku
        if (decymal < 0){
            binar[0] = 1;
        }
        else {
            binar[0] = 0;
        }

        int iterator = 31;
        //konwersja
        while (iterator >= 0){
            if(decymal % 2 == 0){
                binar[iterator] = 0;
            }
            else{
                binar[iterator] = 1;
            }
            decymal /= 2;
            iterator--;
        }


        //wydrukuj do konsoli:
        System.out.print(binar[0] + ".");
        int niezero = 1;
        while(binar[niezero] == 0){
            niezero++;
        }
        for (int i = niezero; i < binar.length; i++) {
            System.out.print(binar[i]);
        }
    }


}
// dlaczego skaner nie czyta minusa z konsoli?