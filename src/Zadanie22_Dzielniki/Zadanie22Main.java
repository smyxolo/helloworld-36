package Zadanie22_Dzielniki;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie22Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        ArrayList<Integer> tablica = new ArrayList<>();

        System.out.println("Podaj liczbe do sprawdzenia dzielnikow: ");
        int liczba = skan.nextInt();


        //podejscie z arraylistem
        for (int i = 1; i <= liczba; i++){
            if(liczba % i == 0){
                tablica.add(i);
            }
        }
        System.out.println("Dzielniki liczby " + liczba + " to:");
        System.out.println(tablica);
        System.out.println();

        //podejscie ze stringiem
        String dzielniki = "";
        for(int i = 1; i <=  liczba; i++){
            if(liczba % i == 0){
                dzielniki += (i + " ");

            }
        }
        System.out.println("Dzielniki liczby " + liczba + " to:");
        System.out.println(dzielniki);
        System.out.println();

        //podejscie z wypisywaniem na biezaco
        System.out.println();
        System.out.println("Dzielniki liczby " + liczba + " to:");
        for(int i = 1; i <=  liczba; i++){
            if(liczba % i == 0){
                System.out.print(i + " ");

            }
        }
        System.out.println();
    }
}
