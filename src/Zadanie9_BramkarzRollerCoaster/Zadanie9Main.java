package Zadanie9_BramkarzRollerCoaster;


import Zadanie5_RollerCoaster.MainZadanie5;

import java.util.Scanner;

public class Zadanie9Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        System.out.println("Podaj swoj wiek: ");
        int wiek = scanner.nextInt();

        System.out.println("Podaj swoją wagę: ");
        int waga = scanner.nextInt();

        System.out.println("Podaj swoj wzrost: ");
        int wzrost = scanner.nextInt();


        MainZadanie5.checkParameters(waga, wzrost, wiek);
        System.out.println();
        System.out.println("_____________inne_podejscie____________");

        // drugie podejście______________________________________

        // kolejnosc: !, &&, ||.

        String wchodzisz = (waga <= 180 && wzrost >= 150 &&(wiek >= 10 && wiek < 80)) ?
                "Witamy na pokladzie!" : "Niestety nie możesz wejść ponieważ: ";
        System.out.println(wchodzisz);

        boolean duzaWaga = (waga > 180);
        boolean malyWzrost = (wzrost < 150);
        boolean mlodyWiek = (wiek < 10);
        boolean starczyWiek = (wiek > 80);

        if(duzaWaga){
            System.out.println("- wazysz zbyt duzo");
        }
        if(malyWzrost){
            System.out.println("- jestes zbyt niski");
        }
        if(mlodyWiek){
            System.out.println("- jestes zbyt mlody");
        }
        if(starczyWiek){
            System.out.println("- nie wpuszczamy osob w tak powaznym wieku");
        }
    }
}
